document.getElementById("hello_text").textContent = "はじめてのJavascript";

var count = 0
var cells;

// ブロックのパターン
var blocks = {
    i: {
        class: "i",
        pattern: [
            [1, 1, 1, 1]
        ]
    },
    o: {
        class: "o",
        pattern: [
            [1, 1],
            [1, 1]
        ]
    },
    t: {
        class: "t",
        pattern: [
            [0, 1, 0],
            [1, 1, 1]
        ]
    },
    s: {
        class: "s",
        pattern: [
            [0, 1, 1],
            [1, 1, 0]
        ]
    },
    z: {
        class: "z",
        pattern: [
            [1, 1, 0],
            [0, 1, 1]
        ]
    },
    j: {
        class: "j",
        pattern: [
            [1, 0, 0],
            [1, 1, 1]
        ]
    },
    l: {
        class: "l",
        pattern: [
            [0, 0, 1],
            [1, 1, 1]
        ]
    }
};

loadTable();
setInterval(function() {
    count++;
    document.getElementById("hello_text").textContent = "はじめてのJavascript(" + count + ")";
    if (hasFallingBlock()) {
        fallBlocks();     
    } else {
        deleteRow();
        isGameOver();
        generateBlock();
    }
}, 1000)

function isGameOver() {
    // ブロックが積み上がりっていないかのチェック
    for (var row = 0; row < 2; row++) {
        for (var col = 0; col < 10; col++) {
            if (cells[row][col].className !== "") {
                alert("game over");
            }
        }
    }
}

function loadTable() {
    cells = [];
    var index = 0;
    var td_array = document.getElementsByTagName("td");
    for (var row = 0; row < 20; row++) {
        cells[row] = [];
        for (var col = 0; col < 10; col++) {
            cells[row][col] = td_array[index];
            index++;
        }
    }
}

function fallBlocks() {
    // 1. ブロックが底についていないか判定
    for (var col = 0; col < 10; col++) {
        if (cells[19][col].blockNum === fallingBlockNum) {
            isFalling = false;
            return; // 一番下の行にブロックがいるので落とさない
        }
    }
    // 2. 1マス下に別のブロックがあるか判定
    for (var row = 18; row >= 0; row--) {
        for (var col = 0; col < 10; col++) {
            if (cells[row][col].blockNum === fallingBlockNum) {
                if (cells[row + 1][col].className !== "" && cells[row + 1][col].blockNum !== fallingBlockNum) {
                    isFalling = false;
                    // 一つ下のマスに別のブロックがあるので落とさない
                    return;
                }
            }
        }
    }
    // 下から二番目の行から繰り返しクラスを下げていく
    for (var row = 18; row >= 0; row--) {
        for (var col = 0; col < 10; col++) {
            if (cells[row][col].blockNum === fallingBlockNum) {
                cells[row + 1][col].className = cells[row][col].className;
                cells[row + 1][col].blockNum = cells[row][col].blockNum;
                cells[row][col].className = "";
                cells[row][col].blockNum = null;
            }
        }
    }
}

var isFalling = false;

function hasFallingBlock() {
    console.log(isFalling);
    return isFalling;
}

function deleteRow() {
    // 揃っている行を消す
    for (var row = 19; row >= 0; row--) {
        var canDelete = true;
        for (var col = 0; col < 10; col++) {
            if (cells[row][col].className === "") {
                canDelete = false;
            }
        }
        if (canDelete) {
            // 1行消す
            console.log("delete :" + row);
            for (var col = 0; col < 10; col++) {
                cells[row][col].className = "";
            }
            // 上の行ブロックを全て1マス落とす
            for (var downRow = row - 1; downRow >= 0; downRow--) {
                console.log(downRow);
                for (var col = 0; col < 10; col++) {
                    console.log("downRow :" + downRow);
                    cells[downRow + 1][col].className = cells[downRow][col].className;
                    cells[downRow + 1][col].blockNum = cells[downRow][col].blockNum;
                    cells[downRow][col].className = "";
                    cells[downRow][col].blockNum = null;
                    
                }
            }
        }
    }
}

var fallingBlockNum = 0;

function generateBlock() {
    // ランダムにブロックを生成する
    // 1. ブロックパターンからランダムに一つパターンを選ぶ
    var keys = Object.keys(blocks);
    var nextBlockKey = keys[Math.floor(Math.random() * keys.length)];
    var nextBlock = blocks[nextBlockKey];
    var nextFallingBlockNum = fallingBlockNum + 1; // 各ブロック固有の番号
    // 2. 選んだパターンをもとにブロックを配置する
    var pattern = nextBlock.pattern;
    for (var row = 0; row < pattern.length; row++) {
        for (var col = 0; col < pattern[row].length; col++) {
            if (pattern[row][col]) {
                cells[row][col + 3].className = nextBlock.class;
                cells[row][col + 3].blockNum = nextFallingBlockNum;
            }
        }
    }
    // 3. 落下中のブロックがあるとする
    isFalling = true;
    fallingBlockNum = nextFallingBlockNum;
}

// キーボードイベントを監視する
document.addEventListener("keydown", onKeyDown);

/* ブロックを左右に動かす時に,画面から
   はみ出さないかどうか判定するための変数 */
var canRightMove = true,
    canLeftMove = true;

// キー入力によってそれぞれの関数を呼び出す
function onKeyDown(event) {
    if (event.keyCode === 37 && canLeftMove) {
        moveLeft();
    } else if (event.keyCode === 39 && canRightMove) {
        moveRight();
    } else if (event.keyCode === 13) { // Enterキーで落下スキップ
        moveBottom();
    } 
}

function moveRight() {
    // ブロックを右に移動させる
    for (var row = 0; row < 20; row++) {
        for (var col = 9; col >= 0; col--) {
            if (cells[row][col].blockNum === fallingBlockNum) {
                cells[row][col + 1].className = cells[row][col].className;
                cells[row][col + 1].blockNum = cells[row][col].blockNum;
                cells[row][col].className = "";
                cells[row][col].blockNum = null;

                /* 右に移動した時に画面の右端に接している
                   ブロックがある場合canRightMoveをfalseに */
                if ((col + 1) == 9)
                    canRightMove = false;
                // ブロックを右に移動させた時にcanLeftMoveをtrueに
                if (canLeftMove == false)
                    canLeftMove = true;
            }
        }
    }
}

function moveLeft() {
    // ブロックを左に移動させる
    for (var row = 0; row < 20; row++) {
        for (var col = 0; col < 10; col++) {
            if (cells[row][col].blockNum === fallingBlockNum) {
                cells[row][col - 1].className = cells[row][col].className;
                cells[row][col - 1].blockNum = cells[row][col].blockNum;
                cells[row][col].className = "";
                cells[row][col].blockNum = null;

                // moveRightの処理と同様
                if ((col - 1) == 0)
                    canLeftMove = false;
                if (canRightMove == false)
                    canRightMove = true;
            }
        }
    }
}

function moveBottom() {
    // ブロックを下端に移動させる
    while (hasFallingBlock())
        fallBlocks();
}